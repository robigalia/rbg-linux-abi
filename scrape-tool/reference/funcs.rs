use types::*;

pub fn sys_generic_getppid() -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_brk(brk: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_set_program_attributes(text_start: usize, text_len: usize, bss_start: usize, bss_len: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getdirentries(fd: u32, dirent: UserPtr<struct_osf_dirent>, count: u32, basep: UserPtr<isize>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, off: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_statfs(pathname: UserPtr<i8>, buffer: UserPtr<struct_osf_statfs>, bufsiz: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_stat(name: UserPtr<i8>, buf: UserPtr<struct_osf_stat>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_lstat(name: UserPtr<i8>, buf: UserPtr<struct_osf_stat>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_fstat(fd: i32, buf: UserPtr<struct_osf_stat>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_fstatfs(fd: usize, buffer: UserPtr<struct_osf_statfs>, bufsiz: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_statfs64(pathname: UserPtr<i8>, buffer: UserPtr<struct_osf_statfs64>, bufsiz: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_fstatfs64(fd: usize, buffer: UserPtr<struct_osf_statfs64>, bufsiz: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_mount(typenr: usize, path: UserPtr<i8>, flag: i32, data: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_utsname(name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_getpagesize() -> isize {
    -ENOSYS
}
pub fn sys_alpha_getdtablesize() -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getdomainname(name: UserPtr<i8>, namelen: i32) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_proplist_syscall(code: enum_pl_code, args: UserPtr<union_ pl_args>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_sigstack(uss: UserPtr<struct_sigstack>, uoss: UserPtr<struct_sigstack>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_sysinfo(command: i32, buf: UserPtr<i8>, count: isize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getsysinfo(op: usize, buffer: UserPtr<void>, nbytes: usize, start: UserPtr<i32>, arg: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_setsysinfo(op: usize, buffer: UserPtr<void>, nbytes: usize, start: UserPtr<i32>, arg: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_gettimeofday(tv: UserPtr<struct_timeval32>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_settimeofday(tv: UserPtr<struct_timeval32>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getitimer(which: i32, it: UserPtr<struct_itimerval32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_setitimer(which: i32, in: UserPtr<struct_itimerval32>, out: UserPtr<struct_itimerval32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_utimes(filename: UserPtr<i8>, tvs: UserPtr<struct_timeval32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_select(n: i32, inp: UserPtr<FDSet>, outp: UserPtr<FDSet>, exp: UserPtr<FDSet>, tvp: UserPtr<struct_timeval32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getrusage(who: i32, ru: UserPtr<struct_rusage32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_wait4(pid: i32, ustatus: UserPtr<i32>, options: i32, ur: UserPtr<struct_rusage32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_usleep_thread(sleep: UserPtr<struct_timeval32>, remain: UserPtr<struct_timeval32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_old_adjtimex(txc_p: UserPtr<struct_timex32>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_readv(fd: usize, vector: UserPtr<struct_iovec>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_writev(fd: usize, vector: UserPtr<struct_iovec>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_getpriority(which: i32, who: i32) -> isize {
    -ENOSYS
}
pub fn sys_alpha_getxuid() -> isize {
    -ENOSYS
}
pub fn sys_alpha_getxgid() -> isize {
    -ENOSYS
}
pub fn sys_alpha_getxpid() -> isize {
    -ENOSYS
}
pub fn sys_alpha_pipe() -> isize {
    -ENOSYS
}
pub fn sys_alpha_sethae(val: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_sigprocmask(how: i32, newmask: usize) -> isize {
    -ENOSYS
}
pub fn sys_alpha_osf_sigaction(sig: i32, act: UserPtr<struct_osf_sigaction>, oact: UserPtr<struct_osf_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_alpha_rt_sigaction(sig: i32, act: UserPtr<struct_sigaction>, oact: UserPtr<struct_sigaction>, sigsetsize: usize, restorer: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_arc_settls(user_tls_data_ptr: *mut void) -> isize {
    -ENOSYS
}
pub fn sys_arc_gettls() -> isize {
    -ENOSYS
}
pub fn sys_arc_rt_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_arc_cacheflush(start: u32, sz: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_blackfin_cacheflush(addr: usize, len: usize, op: i32) -> isize {
    -ENOSYS
}
pub fn sys_microblaze_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, pgoff: isize) -> isize {
    -ENOSYS
}
pub fn sys_microblaze_mmap2(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, pgoff: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_mmap2(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, pgoff: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_truncate64(path: UserPtr<i8>, __dummy: usize, a2: usize, a3: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_ftruncate64(fd: usize, __dummy: usize, a2: usize, a3: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_llseek(fd: u32, offset_high: u32, offset_low: u32, result: UserPtr<i64>, origin: u32) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_pread(fd: usize, buf: UserPtr<i8>, count: usize, unused: usize, a4: usize, a5: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_pwrite(fd: u32, buf: UserPtr<i8>, count: usize, unused: u32, a4: u64, a5: u64) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_personality(personality: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_sigaction(sig: i32, act: UserPtr<struct_sigaction>, oact: UserPtr<struct_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_mips_32_sigaction(sig: isize, act: UserPtr<struct_compat_sigaction>, oact: UserPtr<struct_compat_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_mips_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, offset: isize) -> isize {
    -ENOSYS
}
pub fn sys_mips_mmap2(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, pgoff: usize) -> isize {
    -ENOSYS
}
pub fn sys_mips_set_thread_area(addr: usize) -> isize {
    -ENOSYS
}
pub fn sys_sysmips(cmd: isize, arg1: isize, arg2: isize) -> isize {
    -ENOSYS
}
pub fn sys_mips_cachectl(addr: *mut i8, nbytes: i32, op: i32) -> isize {
    -ENOSYS
}
pub fn sys_mips_cacheflush(addr: usize, bytes: usize, cache: u32) -> isize {
    -ENOSYS
}
pub fn sys_powerpc_spu_create(name: UserPtr<i8>, flags: u32, mode: u16, neighbor_fd: i32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_chown16(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_lchown16(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fchown16(fd: u32, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setregid16(rgid: u16, egid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setgid16(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setreuid16(ruid: u16, euid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setuid16(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setresuid16(ruid: u16, euid: u16, suid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getresuid16(ruidp: UserPtr<u16>, euidp: UserPtr<u16>, suidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setresgid16(rgid: u16, egid: u16, sgid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getresgid16(rgidp: UserPtr<u16>, egidp: UserPtr<u16>, sgidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setfsuid16(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setfsgid16(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getgroups16(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_setgroups16(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getuid16() -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_geteuid16() -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getgid16() -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_getegid16() -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_ipc(call: u32, first: i32, second: u32, third: u32, ptr: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_truncate64(path: UserPtr<i8>, high: u32, low: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_ftruncate64(fd: u32, high: u32, low: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_pread64(fd: u32, ubuf: UserPtr<i8>, count: u32, high: u32, low: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_pwrite64(fd: u32, ubuf: UserPtr<i8>, count: u32, high: u32, low: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_readahead(fd: i32, high: u32, low: u32, count: i32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_stat64(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64_emu31>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_lstat64(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64_emu31>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fstat64(fd: u32, statbuf: UserPtr<struct_stat64_emu31>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fstatat64(dfd: u32, filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64_emu31>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_old_mmap(arg: UserPtr<struct_mmap_arg_struct_emu31>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_mmap2(arg: UserPtr<struct_mmap_arg_struct_emu31>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_read(fd: u32, buf: UserPtr<i8>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_write(fd: u32, buf: UserPtr<i8>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fadvise64(fd: i32, high: u32, low: u32, len: u32, advise: i32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fadvise64_64(args: UserPtr<struct_fadvise64_64_args>) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_sync_file_range(fd: i32, offhigh: u32, offlow: u32, nhigh: u32, nlow: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_compat_s390_fallocate(fd: i32, mode: i32, offhigh: u32, offlow: u32, lenhigh: u32, lenlow: u32) -> isize {
    -ENOSYS
}
pub fn sys_s390_compat_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_s390_compat_rt_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_s390_runtime_instr(command: i32) -> isize {
    -ENOSYS
}
pub fn sys_s390_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_s390_rt_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_s390_mmap2(arg: UserPtr<struct_s390_mmap_arg_struct>) -> isize {
    -ENOSYS
}
pub fn sys_s390_ipc(call: u32, first: i32, second: usize, third: usize, ptr: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_s390_personality(personality: u32) -> isize {
    -ENOSYS
}
pub fn sys_s390_pci_mmio_write(mmio_addr: usize, user_buffer: UserPtr<void>, length: usize) -> isize {
    -ENOSYS
}
pub fn sys_s390_pci_mmio_read(mmio_addr: usize, user_buffer: UserPtr<void>, length: usize) -> isize {
    -ENOSYS
}
pub fn sys_compat_sparc_sigaction(sig: i32, act: UserPtr<struct_compat_old_sigaction>, oact: UserPtr<struct_compat_old_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_sparc_compat_rt_sigaction(sig: i32, act: UserPtr<struct_compat_sigaction>, oact: UserPtr<struct_compat_sigaction>, restorer: UserPtr<void>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_sparc_sigaction(sig: i32, act: UserPtr<struct_old_sigaction>, oact: UserPtr<struct_old_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_sparc_rt_sigaction(sig: i32, act: UserPtr<struct_sigaction>, oact: UserPtr<struct_sigaction>, restorer: UserPtr<void>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_sparc_pipe_real(regs: *mut struct_pt_regs) -> isize {
    -ENOSYS
}
pub fn sys_sparc_ipc(call: u32, first: i32, second: usize, third: usize, ptr: UserPtr<void>, fifth: isize) -> isize {
    -ENOSYS
}
pub fn sys_sparc64_personality(personality: usize) -> isize {
    -ENOSYS
}
pub fn sys_sparc_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, off: usize) -> isize {
    -ENOSYS
}
pub fn sys_sparc_64_munmap(addr: usize, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_sparc_64_mremap(addr: usize, old_len: usize, new_len: usize, flags: usize, new_addr: usize) -> isize {
    -ENOSYS
}
pub fn sys_sparc_getdomainname(name: UserPtr<i8>, len: i32) -> isize {
    -ENOSYS
}
pub fn sys_sparc_utrap_install(type: i32, new_p: *mut u8, new_d: *mut u8, old_p: UserPtr<*mut u8>, old_d: UserPtr<*mut u8>) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_truncate64(filename: UserPtr<i8>, dummy: u32, low: u32, high: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_ftruncate64(fd: u32, dummy: u32, low: u32, high: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_pread64(fd: u32, ubuf: UserPtr<i8>, count: usize, dummy: u32, low: u32, high: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_pwrite64(fd: u32, ubuf: UserPtr<i8>, count: usize, dummy: u32, low: u32, high: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_sync_file_range2(fd: i32, flags: u32, offset_lo: u32, offset_hi: u32, nbytes_lo: u32, nbytes_hi: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_fallocate(fd: i32, mode: i32, offset_lo: u32, offset_hi: u32, len_lo: u32, len_hi: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_compat_llseek(fd: u32, offset_high: u32, offset_low: u32, result: UserPtr<i64>, origin: u32) -> isize {
    -ENOSYS
}
pub fn sys_tile_rt_sigreturn() -> isize {
    -ENOSYS
}
pub fn sys_tile_cacheflush(addr: usize, len: usize, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_tile_mmap2(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, off_4k: usize) -> isize {
    -ENOSYS
}
pub fn sys_tile_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, offset: isize) -> isize {
    -ENOSYS
}
pub fn sys_tile_cmpxchg_badaddr(address: usize) -> isize {
    -ENOSYS
}
pub fn sys_x86_iopl(level: u32) -> isize {
    -ENOSYS
}
pub fn sys_x86_mmap(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, off: usize) -> isize {
    -ENOSYS
}
pub fn sys_x86_set_thread_area(u_info: UserPtr<struct_user_desc>) -> isize {
    -ENOSYS
}
pub fn sys_x86_get_thread_area(u_info: UserPtr<struct_user_desc>) -> isize {
    -ENOSYS
}
pub fn sys_x86_vm86old(user_vm86: UserPtr<struct_vm86_struct>) -> isize {
    -ENOSYS
}
pub fn sys_x86_vm86(cmd: usize, arg: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_ioprio_set(which: i32, who: i32, ioprio: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_ioprio_get(which: i32, who: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getrandom(buf: UserPtr<i8>, count: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_pciconfig_read(bus: usize, dfn: usize, off: usize, len: usize, buf: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_pciconfig_write(bus: usize, dfn: usize, off: usize, len: usize, buf: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_io_setup(nr_events: u32, ctxp: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_io_destroy(ctx: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_io_submit(ctx_id: usize, nr: isize, iocbpp: UserPtr<UserPtr<struct_iocb>>) -> isize {
    -ENOSYS
}
pub fn sys_generic_io_cancel(ctx_id: usize, iocb: UserPtr<struct_iocb>, result: UserPtr<struct_io_event>) -> isize {
    -ENOSYS
}
pub fn sys_generic_io_getevents(ctx_id: usize, min_nr: isize, nr: isize, events: UserPtr<struct_io_event>, timeout: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_bdflush(func: i32, data: isize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_utime(filename: UserPtr<i8>, t: UserPtr<struct_compat_utimbuf>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_utimensat(dfd: u32, filename: UserPtr<i8>, t: UserPtr<struct_compat_timespec>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_futimesat(dfd: u32, filename: UserPtr<i8>, t: UserPtr<struct_compat_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_utimes(filename: UserPtr<i8>, t: UserPtr<struct_compat_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_newstat(filename: UserPtr<i8>, statbuf: UserPtr<struct_compat_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_newlstat(filename: UserPtr<i8>, statbuf: UserPtr<struct_compat_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_newfstatat(dfd: u32, filename: UserPtr<i8>, statbuf: UserPtr<struct_compat_stat>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_newfstat(fd: u32, statbuf: UserPtr<struct_compat_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_statfs(pathname: UserPtr<i8>, buf: UserPtr<struct_compat_statfs>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_fstatfs(fd: u32, buf: UserPtr<struct_compat_statfs>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_statfs64(pathname: UserPtr<i8>, sz: u32, buf: UserPtr<struct_compat_statfs64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_fstatfs64(fd: u32, sz: u32, buf: UserPtr<struct_compat_statfs64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ustat(dev: u32, u: UserPtr<struct_compat_ustat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_fcntl64(fd: u32, cmd: u32, arg: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_fcntl(fd: u32, cmd: u32, arg: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_io_setup(nr_reqs: u32, ctx32p: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_io_getevents(ctx_id: u32, min_nr: i32, nr: i32, events: UserPtr<struct_io_event>, timeout: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_io_submit(ctx_id: u32, nr: i32, iocb: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mount(dev_name: UserPtr<i8>, dir_name: UserPtr<i8>, type: UserPtr<i8>, flags: u32, data: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_old_readdir(fd: u32, dirent: UserPtr<struct_compat_old_linux_dirent>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getdents(fd: u32, dirent: UserPtr<struct_compat_linux_dirent>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getdents64(fd: u32, dirent: UserPtr<struct_linux_dirent64>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_open(filename: UserPtr<i8>, flags: i32, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_openat(dfd: i32, filename: UserPtr<i8>, flags: i32, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_select(n: i32, inp: UserPtr<u32>, outp: UserPtr<u32>, exp: UserPtr<u32>, tvp: UserPtr<struct_compat_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_old_select(arg: UserPtr<struct_compat_sel_arg_struct>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_pselect6(n: i32, inp: UserPtr<u32>, outp: UserPtr<u32>, exp: UserPtr<u32>, tsp: UserPtr<struct_compat_timespec>, sig: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ppoll(ufds: UserPtr<struct_pollfd>, nfds: u32, tsp: UserPtr<struct_compat_timespec>, sigmask: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_open_by_handle_at(mountdirfd: i32, handle: UserPtr<struct_file_handle>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ioctl(fd: u32, cmd: u32, arg32: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getcwd(buf: UserPtr<i8>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_lookup_dcookie(cookie64: u64, buf: UserPtr<i8>, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_lookup_dcookie(w0: u32, w1: u32, buf: UserPtr<i8>, len: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_eventfd2(count: u32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_eventfd(count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_epoll_create1(flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_epoll_create(size: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_epoll_ctl(epfd: i32, op: i32, fd: i32, event: UserPtr<struct_epoll_event>) -> isize {
    -ENOSYS
}
pub fn sys_generic_epoll_wait(epfd: i32, events: UserPtr<struct_epoll_event>, maxevents: i32, timeout: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_epoll_pwait(epfd: i32, events: UserPtr<struct_epoll_event>, maxevents: i32, timeout: i32, sigmask: UserPtr<SigSet>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_epoll_pwait(epfd: i32, events: UserPtr<struct_epoll_event>, maxevents: i32, timeout: i32, sigmask: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_uselib(library: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_execve(filename: UserPtr<i8>, argv: UserPtr<Const<UserPtr<i8>>>, envp: UserPtr<Const<UserPtr<i8>>>) -> isize {
    -ENOSYS
}
pub fn sys_generic_execveat(fd: i32, filename: UserPtr<i8>, argv: UserPtr<Const<UserPtr<i8>>>, envp: UserPtr<Const<UserPtr<i8>>>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_execve(filename: UserPtr<i8>, argv: UserPtr<u32>, envp: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_execveat(fd: i32, filename: UserPtr<i8>, argv: UserPtr<u32>, envp: UserPtr<u32>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fcntl(fd: u32, cmd: u32, arg: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_fcntl64(fd: u32, cmd: u32, arg: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_name_to_handle_at(dfd: i32, name: UserPtr<i8>, handle: UserPtr<struct_file_handle>, mnt_id: UserPtr<i32>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_open_by_handle_at(mountdirfd: i32, handle: UserPtr<struct_file_handle>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_dup3(oldfd: u32, newfd: u32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_dup2(oldfd: u32, newfd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_dup(fildes: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sysfs(option: i32, arg1: usize, arg2: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_ioctl(fd: u32, cmd: u32, arg: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_flock(fd: u32, cmd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_mknodat(dfd: i32, filename: UserPtr<i8>, mode: u16, dev: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_mknod(filename: UserPtr<i8>, mode: u16, dev: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_mkdirat(dfd: i32, pathname: UserPtr<i8>, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_mkdir(pathname: UserPtr<i8>, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_rmdir(pathname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_unlinkat(dfd: i32, pathname: UserPtr<i8>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_unlink(pathname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_symlinkat(oldname: UserPtr<i8>, newdfd: i32, newname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_symlink(oldname: UserPtr<i8>, newname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_linkat(olddfd: i32, oldname: UserPtr<i8>, newdfd: i32, newname: UserPtr<i8>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_link(oldname: UserPtr<i8>, newname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_renameat2(olddfd: i32, oldname: UserPtr<i8>, newdfd: i32, newname: UserPtr<i8>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_renameat(olddfd: i32, oldname: UserPtr<i8>, newdfd: i32, newname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_rename(oldname: UserPtr<i8>, newname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_umount(name: UserPtr<i8>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_oldumount(name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mount(dev_name: UserPtr<i8>, dir_name: UserPtr<i8>, type: UserPtr<i8>, flags: usize, data: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_pivot_root(new_root: UserPtr<i8>, put_old: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fanotify_init(flags: u32, event_f_flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fanotify_mark(fanotify_fd: i32, flags: u32, mask: u64, dfd: i32, pathname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_fanotify_mark(fanotify_fd: i32, flags: u32, mask0: u32, mask1: u32, dfd: i32, pathname: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_inotify_init1(flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_inotify_init() -> isize {
    -ENOSYS
}
pub fn sys_generic_inotify_add_watch(fd: i32, pathname: UserPtr<i8>, mask: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_inotify_rm_watch(fd: i32, wd: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_truncate(path: UserPtr<i8>, length: isize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_truncate(path: UserPtr<i8>, length: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_ftruncate(fd: u32, length: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ftruncate(fd: u32, length: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_truncate64(path: UserPtr<i8>, length: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_ftruncate64(fd: u32, length: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_fallocate(fd: i32, mode: i32, offset: i64, len: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_faccessat(dfd: i32, filename: UserPtr<i8>, mode: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_access(filename: UserPtr<i8>, mode: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_chdir(filename: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchdir(fd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_chroot(filename: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchmod(fd: u32, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchmodat(dfd: i32, filename: UserPtr<i8>, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_chmod(filename: UserPtr<i8>, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchownat(dfd: i32, filename: UserPtr<i8>, user: u16, group: u16, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_chown(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_lchown(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchown(fd: u32, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_open(filename: UserPtr<i8>, flags: i32, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_openat(dfd: i32, filename: UserPtr<i8>, flags: i32, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_creat(pathname: UserPtr<i8>, mode: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_close(fd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_vhangup() -> isize {
    -ENOSYS
}
pub fn sys_generic_pipe2(fildes: UserPtr<i32>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_pipe(fildes: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_quotactl(cmd: u32, special: UserPtr<i8>, id: u32, addr: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_lseek(fd: u32, offset: isize, whence: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_lseek(fd: u32, offset: i32, whence: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_llseek(fd: u32, offset_high: usize, offset_low: usize, result: UserPtr<i64>, whence: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_read(fd: u32, buf: UserPtr<i8>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_write(fd: u32, buf: UserPtr<i8>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_pread64(fd: u32, buf: UserPtr<i8>, count: usize, pos: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_pwrite64(fd: u32, buf: UserPtr<i8>, count: usize, pos: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_readv(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_writev(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_preadv(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize, pos_l: usize, pos_h: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_preadv2(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize, pos_l: usize, pos_h: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_pwritev(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize, pos_l: usize, pos_h: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_pwritev2(fd: usize, vec: UserPtr<struct_iovec>, vlen: usize, pos_l: usize, pos_h: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_readv(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_preadv64(fd: usize, vec: UserPtr<struct_compat_iovec>, vlen: usize, pos: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_preadv(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32, pos_low: u32, pos_high: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_preadv2(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32, pos_low: u32, pos_high: u32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_writev(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_pwritev64(fd: usize, vec: UserPtr<struct_compat_iovec>, vlen: usize, pos: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_pwritev(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32, pos_low: u32, pos_high: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_pwritev2(fd: u32, vec: UserPtr<struct_compat_iovec>, vlen: u32, pos_low: u32, pos_high: u32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sendfile(out_fd: i32, in_fd: i32, offset: UserPtr<isize>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_sendfile64(out_fd: i32, in_fd: i32, offset: UserPtr<i64>, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sendfile(out_fd: i32, in_fd: i32, offset: UserPtr<i32>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sendfile64(out_fd: i32, in_fd: i32, offset: UserPtr<i64>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_copy_file_range(fd_in: i32, off_in: UserPtr<i64>, fd_out: i32, off_out: UserPtr<i64>, len: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_old_readdir(fd: u32, dirent: UserPtr<struct_old_linux_dirent>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getdents(fd: u32, dirent: UserPtr<struct_linux_dirent>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getdents64(fd: u32, dirent: UserPtr<struct_linux_dirent64>, count: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_select(n: i32, inp: UserPtr<FDSet>, outp: UserPtr<FDSet>, exp: UserPtr<FDSet>, tvp: UserPtr<struct_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_pselect6(n: i32, inp: UserPtr<FDSet>, outp: UserPtr<FDSet>, exp: UserPtr<FDSet>, tsp: UserPtr<struct_timespec>, sig: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_old_select(arg: UserPtr<struct_sel_arg_struct>) -> isize {
    -ENOSYS
}
pub fn sys_generic_poll(ufds: UserPtr<struct_pollfd>, nfds: u32, timeout_msecs: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_ppoll(ufds: UserPtr<struct_pollfd>, nfds: u32, tsp: UserPtr<struct_timespec>, sigmask: UserPtr<SigSet>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_signalfd4(ufd: i32, user_mask: UserPtr<SigSet>, sizemask: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_signalfd(ufd: i32, user_mask: UserPtr<SigSet>, sizemask: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_signalfd4(ufd: i32, sigmask: UserPtr<CompatSigSet>, sigsetsize: u32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_signalfd(ufd: i32, sigmask: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_vmsplice(fd: i32, iov: UserPtr<struct_iovec>, nr_segs: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_vmsplice(fd: i32, iov32: UserPtr<struct_compat_iovec>, nr_segs: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_splice(fd_in: i32, off_in: UserPtr<i64>, fd_out: i32, off_out: UserPtr<i64>, len: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_tee(fdin: i32, fdout: i32, len: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_stat(filename: UserPtr<i8>, statbuf: UserPtr<struct___old_kernel_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_lstat(filename: UserPtr<i8>, statbuf: UserPtr<struct___old_kernel_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fstat(fd: u32, statbuf: UserPtr<struct___old_kernel_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_newstat(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_newlstat(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_newfstatat(dfd: i32, filename: UserPtr<i8>, statbuf: UserPtr<struct_stat>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_newfstat(fd: u32, statbuf: UserPtr<struct_stat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_readlinkat(dfd: i32, pathname: UserPtr<i8>, buf: UserPtr<i8>, bufsiz: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_readlink(path: UserPtr<i8>, buf: UserPtr<i8>, bufsiz: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_stat64(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_lstat64(filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fstat64(fd: usize, statbuf: UserPtr<struct_stat64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fstatat64(dfd: i32, filename: UserPtr<i8>, statbuf: UserPtr<struct_stat64>, flag: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_statfs(pathname: UserPtr<i8>, buf: UserPtr<struct_statfs>) -> isize {
    -ENOSYS
}
pub fn sys_generic_statfs64(pathname: UserPtr<i8>, sz: usize, buf: UserPtr<struct_statfs64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fstatfs(fd: u32, buf: UserPtr<struct_statfs>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fstatfs64(fd: u32, sz: usize, buf: UserPtr<struct_statfs64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_ustat(dev: u32, ubuf: UserPtr<struct_ustat>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sync() -> isize {
    -ENOSYS
}
pub fn sys_generic_syncfs(fd: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fsync(fd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fdatasync(fd: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sync_file_range(fd: i32, offset: i64, nbytes: i64, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sync_file_range2(fd: i32, flags: u32, offset: i64, nbytes: i64) -> isize {
    -ENOSYS
}
pub fn sys_generic_timerfd_create(clockid: i32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_timerfd_settime(ufd: i32, flags: i32, utmr: UserPtr<struct_itimerspec>, otmr: UserPtr<struct_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_timerfd_gettime(ufd: i32, otmr: UserPtr<struct_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_timerfd_settime(ufd: i32, flags: i32, utmr: UserPtr<struct_compat_itimerspec>, otmr: UserPtr<struct_compat_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_timerfd_gettime(ufd: i32, otmr: UserPtr<struct_compat_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_userfaultfd(flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_utime(filename: UserPtr<i8>, times: UserPtr<struct_utimbuf>) -> isize {
    -ENOSYS
}
pub fn sys_generic_utimensat(dfd: i32, filename: UserPtr<i8>, utimes: UserPtr<struct_timespec>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_futimesat(dfd: i32, filename: UserPtr<i8>, utimes: UserPtr<struct_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_utimes(filename: UserPtr<i8>, utimes: UserPtr<struct_timeval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setxattr(pathname: UserPtr<i8>, name: UserPtr<i8>, value: UserPtr<void>, size: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_lsetxattr(pathname: UserPtr<i8>, name: UserPtr<i8>, value: UserPtr<void>, size: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fsetxattr(fd: i32, name: UserPtr<i8>, value: UserPtr<void>, size: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getxattr(pathname: UserPtr<i8>, name: UserPtr<i8>, value: UserPtr<void>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_lgetxattr(pathname: UserPtr<i8>, name: UserPtr<i8>, value: UserPtr<void>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_fgetxattr(fd: i32, name: UserPtr<i8>, value: UserPtr<void>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_listxattr(pathname: UserPtr<i8>, list: UserPtr<i8>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_llistxattr(pathname: UserPtr<i8>, list: UserPtr<i8>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_flistxattr(fd: i32, list: UserPtr<i8>, size: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_removexattr(pathname: UserPtr<i8>, name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_lremovexattr(pathname: UserPtr<i8>, name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fremovexattr(fd: i32, name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ipc(call: u32, first: i32, second: i32, third: u32, ptr: u32, fifth: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_semctl(semid: i32, semnum: i32, cmd: i32, arg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_msgsnd(msqid: i32, msgp: u32, msgsz: i32, msgflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_msgrcv(msqid: i32, msgp: u32, msgsz: i32, msgtyp: i32, msgflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_msgctl(first: i32, second: i32, uptr: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_shmat(shmid: i32, shmaddr: u32, shmflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_shmctl(first: i32, second: i32, uptr: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_semtimedop(semid: i32, tsems: UserPtr<struct_sembuf>, nsops: u32, timeout: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mq_open(u_name: UserPtr<i8>, oflag: i32, mode: CompatMode, u_attr: UserPtr<struct_compat_mq_attr>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mq_timedsend(mqdes: i32, u_msg_ptr: UserPtr<i8>, msg_len: u32, msg_prio: u32, u_abs_timeout: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mq_timedreceive(mqdes: i32, u_msg_ptr: UserPtr<i8>, msg_len: u32, u_msg_prio: UserPtr<u32>, u_abs_timeout: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mq_notify(mqdes: i32, u_notification: UserPtr<struct_compat_sigevent>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mq_getsetattr(mqdes: i32, u_mqstat: UserPtr<struct_compat_mq_attr>, u_omqstat: UserPtr<struct_compat_mq_attr>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_open(u_name: UserPtr<i8>, oflag: i32, mode: u16, u_attr: UserPtr<struct_mq_attr>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_unlink(u_name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_timedsend(mqdes: i32, u_msg_ptr: UserPtr<i8>, msg_len: usize, msg_prio: u32, u_abs_timeout: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_timedreceive(mqdes: i32, u_msg_ptr: UserPtr<i8>, msg_len: usize, u_msg_prio: UserPtr<u32>, u_abs_timeout: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_notify(mqdes: i32, u_notification: UserPtr<struct_sigevent>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mq_getsetattr(mqdes: i32, u_mqstat: UserPtr<struct_mq_attr>, u_omqstat: UserPtr<struct_mq_attr>) -> isize {
    -ENOSYS
}
pub fn sys_generic_msgget(key: i32, msgflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_msgctl(msqid: i32, cmd: i32, buf: UserPtr<struct_msqid_ds>) -> isize {
    -ENOSYS
}
pub fn sys_generic_msgsnd(msqid: i32, msgp: UserPtr<struct_msgbuf>, msgsz: usize, msgflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_msgrcv(msqid: i32, msgp: UserPtr<struct_msgbuf>, msgsz: usize, msgtyp: isize, msgflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_semget(key: i32, nsems: i32, semflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_semctl(semid: i32, semnum: i32, cmd: i32, arg: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_semtimedop(semid: i32, tsops: UserPtr<struct_sembuf>, nsops: u32, timeout: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_semop(semid: i32, tsops: UserPtr<struct_sembuf>, nsops: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_shmget(key: i32, size: usize, shmflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_shmctl(shmid: i32, cmd: i32, buf: UserPtr<struct_shmid_ds>) -> isize {
    -ENOSYS
}
pub fn sys_generic_shmat(shmid: i32, shmaddr: UserPtr<i8>, shmflg: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_shmdt(shmaddr: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_ipc(call: u32, first: i32, second: usize, third: usize, ptr: UserPtr<void>, fifth: isize) -> isize {
    -ENOSYS
}
pub fn sys_generic_acct(name: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_bpf(cmd: i32, uattr: UserPtr<union_ bpf_attr>, size: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_capget(header: UserPtr<CapUserHeader>, dataptr: UserPtr<CapUserData>) -> isize {
    -ENOSYS
}
pub fn sys_generic_capset(header: UserPtr<CapUserHeader>, data: UserPtr<CapUserData>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_gettimeofday(tv: UserPtr<struct_compat_timeval>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_settimeofday(tv: UserPtr<struct_compat_timeval>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_nanosleep(rqtp: UserPtr<struct_compat_timespec>, rmtp: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getitimer(which: i32, it: UserPtr<struct_compat_itimerval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_setitimer(which: i32, in: UserPtr<struct_compat_itimerval>, out: UserPtr<struct_compat_itimerval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_times(tbuf: UserPtr<struct_compat_tms>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sigpending(set: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sigprocmask(how: i32, nset: UserPtr<u32>, oset: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_setrlimit(resource: u32, rlim: UserPtr<struct_compat_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_old_getrlimit(resource: u32, rlim: UserPtr<struct_compat_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getrlimit(resource: u32, rlim: UserPtr<struct_compat_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_wait4(pid: i32, stat_addr: UserPtr<u32>, options: i32, ru: UserPtr<struct_compat_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_waitid(which: i32, pid: i32, uinfo: UserPtr<struct_compat_siginfo>, options: i32, uru: UserPtr<struct_compat_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sched_setaffinity(pid: i32, len: u32, user_mask_ptr: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sched_getaffinity(pid: i32, len: u32, user_mask_ptr: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_timer_create(which_clock: i32, timer_event_spec: UserPtr<struct_compat_sigevent>, created_timer_id: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_timer_settime(timer_id: i32, flags: i32, new: UserPtr<struct_compat_itimerspec>, old: UserPtr<struct_compat_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_timer_gettime(timer_id: i32, setting: UserPtr<struct_compat_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_clock_settime(which_clock: i32, tp: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_clock_gettime(which_clock: i32, tp: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_clock_adjtime(which_clock: i32, utp: UserPtr<struct_compat_timex>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_clock_getres(which_clock: i32, tp: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_clock_nanosleep(which_clock: i32, flags: i32, rqtp: UserPtr<struct_compat_timespec>, rmtp: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigtimedwait(uthese: UserPtr<CompatSigSet>, uinfo: UserPtr<struct_compat_siginfo>, uts: UserPtr<struct_compat_timespec>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_time(tloc: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_stime(tptr: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_adjtimex(utp: UserPtr<struct_compat_timex>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_move_pages(pid: i32, nr_pages: u32, pages32: UserPtr<u32>, nodes: UserPtr<i32>, status: UserPtr<i32>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_migrate_pages(pid: i32, maxnode: u32, old_nodes: UserPtr<u32>, new_nodes: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sched_rr_get_interval(pid: i32, interval: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_perf_event_open(attr_uptr: UserPtr<struct_perf_event_attr>, pid: i32, cpu: i32, group_fd: i32, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_personality(personality: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_exit(error_code: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_exit_group(error_code: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_waitid(which: i32, upid: i32, infop: UserPtr<struct_siginfo>, options: i32, ru: UserPtr<struct_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_wait4(upid: i32, stat_addr: UserPtr<i32>, options: i32, ru: UserPtr<struct_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_waitpid(pid: i32, stat_addr: UserPtr<i32>, options: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_set_tid_address(tidptr: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_fork() -> isize {
    -ENOSYS
}
pub fn sys_generic_vfork() -> isize {
    -ENOSYS
}
pub fn sys_generic_unshare(unshare_flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_set_robust_list(head: UserPtr<struct_robust_list_head>, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_get_robust_list(pid: i32, head_ptr: UserPtr<UserPtr<struct_robust_list_head>>, len_ptr: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_futex(uaddr: UserPtr<u32>, op: i32, val: u32, utime: UserPtr<struct_timespec>, uaddr2: UserPtr<u32>, val3: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_set_robust_list(head: UserPtr<struct_compat_robust_list_head>, len: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_get_robust_list(pid: i32, head_ptr: UserPtr<u32>, len_ptr: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_futex(uaddr: UserPtr<u32>, op: i32, val: u32, utime: UserPtr<struct_compat_timespec>, uaddr2: UserPtr<u32>, val3: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getgroups(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setgroups(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_kcmp(pid1: i32, pid2: i32, type: i32, idx1: usize, idx2: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_kexec_load(entry: usize, nr_segments: usize, segments: UserPtr<struct_kexec_segment>, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_kexec_load(entry: u32, nr_segments: u32, segments: UserPtr<struct_compat_kexec_segment>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_kexec_file_load(kernel_fd: i32, initrd_fd: i32, cmdline_len: usize, cmdline_ptr: UserPtr<i8>, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_membarrier(cmd: i32, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_delete_module(name_user: UserPtr<i8>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_init_module(umod: UserPtr<void>, len: usize, uargs: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_finit_module(fd: i32, uargs: UserPtr<i8>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setns(fd: i32, nstype: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_syslog(type: i32, buf: UserPtr<i8>, len: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_ptrace(request: isize, pid: isize, addr: usize, data: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_ptrace(request: i32, pid: i32, addr: i32, data: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_reboot(magic1: i32, magic2: i32, cmd: u32, arg: UserPtr<void>) -> isize {
    -ENOSYS
}
pub fn sys_generic_nice(increment: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_setscheduler(pid: i32, policy: i32, param: UserPtr<struct_sched_param>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_setparam(pid: i32, param: UserPtr<struct_sched_param>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_setattr(pid: i32, uattr: UserPtr<struct_sched_attr>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_getscheduler(pid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_getparam(pid: i32, param: UserPtr<struct_sched_param>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_getattr(pid: i32, uattr: UserPtr<struct_sched_attr>, size: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_setaffinity(pid: i32, len: u32, user_mask_ptr: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_getaffinity(pid: i32, len: u32, user_mask_ptr: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_yield() -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_get_priority_max(policy: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_get_priority_min(policy: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sched_rr_get_interval(pid: i32, interval: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_seccomp(op: u32, flags: u32, uargs: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_restart_syscall() -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigprocmask(how: i32, nset: UserPtr<SigSet>, oset: UserPtr<SigSet>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigprocmask(how: i32, nset: UserPtr<CompatSigSet>, oset: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigpending(uset: UserPtr<SigSet>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigpending(uset: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigtimedwait(uthese: UserPtr<SigSet>, uinfo: UserPtr<SigInfo>, uts: UserPtr<struct_timespec>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_kill(pid: i32, sig: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_tgkill(tgid: i32, pid: i32, sig: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_tkill(pid: i32, sig: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigqueueinfo(pid: i32, sig: i32, uinfo: UserPtr<SigInfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigqueueinfo(pid: i32, sig: i32, uinfo: UserPtr<struct_compat_siginfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_tgsigqueueinfo(tgid: i32, pid: i32, sig: i32, uinfo: UserPtr<SigInfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_tgsigqueueinfo(tgid: i32, pid: i32, sig: i32, uinfo: UserPtr<struct_compat_siginfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sigaltstack(uss: UserPtr<Stack>, uoss: UserPtr<Stack>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sigaltstack(uss_ptr: UserPtr<CompatStack>, uoss_ptr: UserPtr<CompatStack>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sigpending(set: UserPtr<isize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sigprocmask(how: i32, nset: UserPtr<isize>, oset: UserPtr<isize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigaction(sig: i32, act: UserPtr<struct_sigaction>, oact: UserPtr<struct_sigaction>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigaction(sig: i32, act: UserPtr<struct_compat_sigaction>, oact: UserPtr<struct_compat_sigaction>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sigaction(sig: i32, act: UserPtr<struct_old_sigaction>, oact: UserPtr<struct_old_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sigaction(sig: i32, act: UserPtr<struct_compat_old_sigaction>, oact: UserPtr<struct_compat_old_sigaction>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sgetmask() -> isize {
    -ENOSYS
}
pub fn sys_generic_ssetmask(newmask: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_signal(sig: i32, handler: UserPtr<u8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_pause() -> isize {
    -ENOSYS
}
pub fn sys_generic_rt_sigsuspend(unewset: UserPtr<SigSet>, sigsetsize: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_rt_sigsuspend(unewset: UserPtr<CompatSigSet>, sigsetsize: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setpriority(which: i32, who: i32, niceval: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getpriority(which: i32, who: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setregid(rgid: u16, egid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setgid(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setreuid(ruid: u16, euid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setuid(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setresuid(ruid: u16, euid: u16, suid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getresuid(ruidp: UserPtr<u16>, euidp: UserPtr<u16>, suidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setresgid(rgid: u16, egid: u16, sgid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getresgid(rgidp: UserPtr<u16>, egidp: UserPtr<u16>, sgidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setfsuid(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setfsgid(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getpid() -> isize {
    -ENOSYS
}
pub fn sys_generic_gettid() -> isize {
    -ENOSYS
}
pub fn sys_generic_getuid() -> isize {
    -ENOSYS
}
pub fn sys_generic_geteuid() -> isize {
    -ENOSYS
}
pub fn sys_generic_getgid() -> isize {
    -ENOSYS
}
pub fn sys_generic_getegid() -> isize {
    -ENOSYS
}
pub fn sys_generic_times(tbuf: UserPtr<struct_tms>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setpgid(pid: i32, pgid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getpgid(pid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getpgrp() -> isize {
    -ENOSYS
}
pub fn sys_generic_getsid(pid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setsid() -> isize {
    -ENOSYS
}
pub fn sys_generic_newuname(name: UserPtr<struct_new_utsname>) -> isize {
    -ENOSYS
}
pub fn sys_generic_uname(name: UserPtr<struct_old_utsname>) -> isize {
    -ENOSYS
}
pub fn sys_generic_olduname(name: UserPtr<struct_oldold_utsname>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sethostname(name: UserPtr<i8>, len: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_gethostname(name: UserPtr<i8>, len: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setdomainname(name: UserPtr<i8>, len: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getrlimit(resource: u32, rlim: UserPtr<struct_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_old_getrlimit(resource: u32, rlim: UserPtr<struct_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_prlimit64(pid: i32, resource: u32, new_rlim: UserPtr<struct_rlimit64>, old_rlim: UserPtr<struct_rlimit64>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setrlimit(resource: u32, rlim: UserPtr<struct_rlimit>) -> isize {
    -ENOSYS
}
pub fn sys_generic_getrusage(who: i32, ru: UserPtr<struct_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getrusage(who: i32, ru: UserPtr<struct_compat_rusage>) -> isize {
    -ENOSYS
}
pub fn sys_generic_umask(mask: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_prctl(option: i32, arg2: usize, arg3: usize, arg4: usize, arg5: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_getcpu(cpup: UserPtr<u32>, nodep: UserPtr<u32>, unused: UserPtr<struct_getcpu_cache>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sysinfo(info: UserPtr<struct_sysinfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sysinfo(info: UserPtr<struct_compat_sysinfo>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sysctl(args: UserPtr<struct___sysctl_args>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sysctl(args: UserPtr<struct_compat_sysctl_args>) -> isize {
    -ENOSYS
}
pub fn sys_generic_nanosleep(rqtp: UserPtr<struct_timespec>, rmtp: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_getitimer(which: i32, value: UserPtr<struct_itimerval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setitimer(which: i32, value: UserPtr<struct_itimerval>, ovalue: UserPtr<struct_itimerval>) -> isize {
    -ENOSYS
}
pub fn sys_generic_timer_create(which_clock: i32, timer_event_spec: UserPtr<struct_sigevent>, created_timer_id: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_timer_gettime(timer_id: i32, setting: UserPtr<struct_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_timer_getoverrun(timer_id: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_timer_settime(timer_id: i32, flags: i32, new_setting: UserPtr<struct_itimerspec>, old_setting: UserPtr<struct_itimerspec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_timer_delete(timer_id: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_clock_settime(which_clock: i32, tp: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_clock_gettime(which_clock: i32, tp: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_clock_adjtime(which_clock: i32, utx: UserPtr<struct_timex>) -> isize {
    -ENOSYS
}
pub fn sys_generic_clock_getres(which_clock: i32, tp: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_clock_nanosleep(which_clock: i32, flags: i32, rqtp: UserPtr<struct_timespec>, rmtp: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_time(tloc: UserPtr<isize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_stime(tptr: UserPtr<isize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_gettimeofday(tv: UserPtr<struct_timeval>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_generic_settimeofday(tv: UserPtr<struct_timeval>, tz: UserPtr<struct_timezone>) -> isize {
    -ENOSYS
}
pub fn sys_generic_adjtimex(txc_p: UserPtr<struct_timex>) -> isize {
    -ENOSYS
}
pub fn sys_generic_alarm(seconds: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_chown16(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_lchown16(filename: UserPtr<i8>, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_fchown16(fd: u32, user: u16, group: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setregid16(rgid: u16, egid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setgid16(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setreuid16(ruid: u16, euid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setuid16(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setresuid16(ruid: u16, euid: u16, suid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getresuid16(ruidp: UserPtr<u16>, euidp: UserPtr<u16>, suidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setresgid16(rgid: u16, egid: u16, sgid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getresgid16(rgidp: UserPtr<u16>, egidp: UserPtr<u16>, sgidp: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setfsuid16(uid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_setfsgid16(gid: u16) -> isize {
    -ENOSYS
}
pub fn sys_generic_getgroups16(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_setgroups16(gidsetsize: i32, grouplist: UserPtr<u16>) -> isize {
    -ENOSYS
}
pub fn sys_generic_getuid16() -> isize {
    -ENOSYS
}
pub fn sys_generic_geteuid16() -> isize {
    -ENOSYS
}
pub fn sys_generic_getgid16() -> isize {
    -ENOSYS
}
pub fn sys_generic_getegid16() -> isize {
    -ENOSYS
}
pub fn sys_generic_fadvise64_64(fd: i32, offset: i64, len: i64, advice: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_fadvise64(fd: i32, offset: i64, len: usize, advice: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_madvise(start: usize, len_in: usize, behavior: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_mbind(start: usize, len: usize, mode: usize, nmask: UserPtr<usize>, maxnode: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_set_mempolicy(mode: i32, nmask: UserPtr<usize>, maxnode: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_migrate_pages(pid: i32, maxnode: usize, old_nodes: UserPtr<usize>, new_nodes: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_get_mempolicy(policy: UserPtr<i32>, nmask: UserPtr<usize>, maxnode: usize, addr: usize, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_get_mempolicy(policy: UserPtr<i32>, nmask: UserPtr<u32>, maxnode: u32, addr: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_set_mempolicy(mode: i32, nmask: UserPtr<u32>, maxnode: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_mbind(start: u32, len: u32, mode: u32, nmask: UserPtr<u32>, maxnode: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_move_pages(pid: i32, nr_pages: usize, pages: UserPtr<UserPtr<void>>, nodes: UserPtr<i32>, status: UserPtr<i32>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_mincore(start: usize, len: usize, vec: UserPtr<u8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_mlock(start: usize, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_mlock2(start: usize, len: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_munlock(start: usize, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_mlockall(flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_munlockall() -> isize {
    -ENOSYS
}
pub fn sys_generic_brk(brk: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_mmap_pgoff(addr: usize, len: usize, prot: usize, flags: usize, fd: usize, pgoff: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_old_mmap(arg: UserPtr<struct_mmap_arg_struct>) -> isize {
    -ENOSYS
}
pub fn sys_generic_munmap(addr: usize, len: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_remap_file_pages(start: usize, size: usize, prot: usize, pgoff: usize, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_mprotect(start: usize, len: usize, prot: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_mremap(addr: usize, old_len: usize, new_len: usize, flags: usize, new_addr: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_msync(start: usize, len: usize, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_process_vm_readv(pid: i32, lvec: UserPtr<struct_iovec>, liovcnt: usize, rvec: UserPtr<struct_iovec>, riovcnt: usize, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_process_vm_writev(pid: i32, lvec: UserPtr<struct_iovec>, liovcnt: usize, rvec: UserPtr<struct_iovec>, riovcnt: usize, flags: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_process_vm_readv(pid: i32, lvec: UserPtr<struct_compat_iovec>, liovcnt: u32, rvec: UserPtr<struct_compat_iovec>, riovcnt: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_process_vm_writev(pid: i32, lvec: UserPtr<struct_compat_iovec>, liovcnt: u32, rvec: UserPtr<struct_compat_iovec>, riovcnt: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_readahead(fd: i32, offset: i64, count: usize) -> isize {
    -ENOSYS
}
pub fn sys_generic_memfd_create(uname: UserPtr<i8>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_swapoff(specialfile: UserPtr<i8>) -> isize {
    -ENOSYS
}
pub fn sys_generic_swapon(specialfile: UserPtr<i8>, swap_flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_setsockopt(fd: i32, level: i32, optname: i32, optval: UserPtr<i8>, optlen: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_getsockopt(fd: i32, level: i32, optname: i32, optval: UserPtr<i8>, optlen: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sendmsg(fd: i32, msg: UserPtr<struct_compat_msghdr>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_sendmmsg(fd: i32, mmsg: UserPtr<struct_compat_mmsghdr>, vlen: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_recvmsg(fd: i32, msg: UserPtr<struct_compat_msghdr>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_recv(fd: i32, buf: UserPtr<void>, len: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_recvfrom(fd: i32, buf: UserPtr<void>, len: u32, flags: u32, addr: UserPtr<struct_sockaddr>, addrlen: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_recvmmsg(fd: i32, mmsg: UserPtr<struct_compat_mmsghdr>, vlen: u32, flags: u32, timeout: UserPtr<struct_compat_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_socketcall(call: i32, args: UserPtr<u32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_socket(family: i32, type: i32, protocol: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_socketpair(family: i32, type: i32, protocol: i32, usockvec: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_bind(fd: i32, umyaddr: UserPtr<struct_sockaddr>, addrlen: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_listen(fd: i32, backlog: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_accept4(fd: i32, upeer_sockaddr: UserPtr<struct_sockaddr>, upeer_addrlen: UserPtr<i32>, flags: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_accept(fd: i32, upeer_sockaddr: UserPtr<struct_sockaddr>, upeer_addrlen: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_connect(fd: i32, uservaddr: UserPtr<struct_sockaddr>, addrlen: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getsockname(fd: i32, usockaddr: UserPtr<struct_sockaddr>, usockaddr_len: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_getpeername(fd: i32, usockaddr: UserPtr<struct_sockaddr>, usockaddr_len: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_sendto(fd: i32, buff: UserPtr<void>, len: usize, flags: u32, addr: UserPtr<struct_sockaddr>, addr_len: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_send(fd: i32, buff: UserPtr<void>, len: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_recvfrom(fd: i32, ubuf: UserPtr<void>, size: usize, flags: u32, addr: UserPtr<struct_sockaddr>, addr_len: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_recv(fd: i32, ubuf: UserPtr<void>, size: usize, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_setsockopt(fd: i32, level: i32, optname: i32, optval: UserPtr<i8>, optlen: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_getsockopt(fd: i32, level: i32, optname: i32, optval: UserPtr<i8>, optlen: UserPtr<i32>) -> isize {
    -ENOSYS
}
pub fn sys_generic_shutdown(fd: i32, how: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sendmsg(fd: i32, msg: UserPtr<struct_user_msghdr>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_sendmmsg(fd: i32, mmsg: UserPtr<struct_mmsghdr>, vlen: u32, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_recvmsg(fd: i32, msg: UserPtr<struct_user_msghdr>, flags: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_recvmmsg(fd: i32, mmsg: UserPtr<struct_mmsghdr>, vlen: u32, flags: u32, timeout: UserPtr<struct_timespec>) -> isize {
    -ENOSYS
}
pub fn sys_generic_socketcall(call: i32, args: UserPtr<usize>) -> isize {
    -ENOSYS
}
pub fn sys_generic_compat_keyctl(option: u32, arg2: u32, arg3: u32, arg4: u32, arg5: u32) -> isize {
    -ENOSYS
}
pub fn sys_generic_add_key(_type: UserPtr<i8>, _description: UserPtr<i8>, _payload: UserPtr<void>, plen: usize, ringid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_request_key(_type: UserPtr<i8>, _description: UserPtr<i8>, _callout_info: UserPtr<i8>, destringid: i32) -> isize {
    -ENOSYS
}
pub fn sys_generic_keyctl(option: i32, arg2: usize, arg3: usize, arg4: usize, arg5: usize) -> isize {
    -ENOSYS
}
