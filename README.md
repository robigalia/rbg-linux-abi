# rbg-linux-abi

[![Crates.io](https://img.shields.io/crates/v/sel4-alloc.svg?style=flat-square)](https://crates.io/crates/sel4-alloc)

[x86 Documentation](https://doc.robigalia.org/i686/sel4_alloc) | [ARM Documentation](https://doc.robigalia.org/ARM/sel4_alloc) 

This crate provides Rust declarations of the Linux kernel system call ABI and
dispatch functions for system calls, as well as a tool for creating such
declarations dispatchers largely automatically from a Linux kernel source
tree.

# Status

Incomplete, under active development.
