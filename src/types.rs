pub struct UserPtr<T> {
    pub userptr: usize,
	pub _ph: ::core::marker::PhantomData<T>,
}
pub struct Const<T> {
    pub const_val: T,
}

#[repr(C)] pub struct struct___old_kernel_stat;
#[repr(C)] pub struct struct___sysctl_args;
#[repr(C)] pub struct struct_compat_iovec;
#[repr(C)] pub struct struct_compat_itimerspec;
#[repr(C)] pub struct struct_compat_itimerval;
#[repr(C)] pub struct struct_compat_kexec_segment;
#[repr(C)] pub struct struct_compat_linux_dirent;
#[repr(C)] pub struct struct_compat_mmsghdr;
#[repr(C)] pub struct struct_compat_mq_attr;
#[repr(C)] pub struct struct_compat_msghdr;
#[repr(C)] pub struct struct_compat_old_linux_dirent;
#[repr(C)] pub struct struct_compat_old_sigaction;
#[repr(C)] pub struct struct_compat_rlimit;
#[repr(C)] pub struct struct_compat_robust_list_head;
#[repr(C)] pub struct struct_compat_rusage;
#[repr(C)] pub struct struct_compat_sel_arg_struct;
#[repr(C)] pub struct struct_compat_sigaction;
#[repr(C)] pub struct struct_compat_sigevent;
#[repr(C)] pub struct struct_compat_siginfo;
#[repr(C)] pub struct struct_compat_stat;
#[repr(C)] pub struct struct_compat_statfs64;
#[repr(C)] pub struct struct_compat_statfs;
#[repr(C)] pub struct struct_compat_sysctl_args;
#[repr(C)] pub struct struct_compat_sysinfo;
#[repr(C)] pub struct struct_compat_timespec;
#[repr(C)] pub struct struct_compat_timeval;
#[repr(C)] pub struct struct_compat_timex;
#[repr(C)] pub struct struct_compat_tms;
#[repr(C)] pub struct struct_compat_ustat;
#[repr(C)] pub struct struct_compat_utimbuf;
#[repr(C)] pub struct struct_epoll_event;
#[repr(C)] pub struct struct_fadvise64_64_args;
#[repr(C)] pub struct struct_file_handle;
#[repr(C)] pub struct struct_getcpu_cache;
#[repr(C)] pub struct struct_io_event;
#[repr(C)] pub struct struct_iocb;
#[repr(C)] pub struct struct_iovec;
#[repr(C)] pub struct struct_itimerspec;
#[repr(C)] pub struct struct_itimerval32;
#[repr(C)] pub struct struct_itimerval;
#[repr(C)] pub struct struct_kexec_segment;
#[repr(C)] pub struct struct_linux_dirent64;
#[repr(C)] pub struct struct_linux_dirent;
#[repr(C)] pub struct struct_mmap_arg_struct;
#[repr(C)] pub struct struct_mmap_arg_struct_emu31;
#[repr(C)] pub struct struct_mmsghdr;
#[repr(C)] pub struct struct_mq_attr;
#[repr(C)] pub struct struct_msgbuf;
#[repr(C)] pub struct struct_msqid_ds;
#[repr(C)] pub struct struct_new_utsname;
#[repr(C)] pub struct struct_old_linux_dirent;
#[repr(C)] pub struct struct_old_sigaction;
#[repr(C)] pub struct struct_old_utsname;
#[repr(C)] pub struct struct_oldold_utsname;
#[repr(C)] pub struct struct_osf_dirent;
#[repr(C)] pub struct struct_osf_sigaction;
#[repr(C)] pub struct struct_osf_stat;
#[repr(C)] pub struct struct_osf_statfs64;
#[repr(C)] pub struct struct_osf_statfs;
#[repr(C)] pub struct struct_perf_event_attr;
#[repr(C)] pub struct struct_pollfd;
#[repr(C)] pub struct struct_pt_regs;
#[repr(C)] pub struct struct_rlimit64;
#[repr(C)] pub struct struct_rlimit;
#[repr(C)] pub struct struct_robust_list_head;
#[repr(C)] pub struct struct_rusage32;
#[repr(C)] pub struct struct_rusage;
#[repr(C)] pub struct struct_s390_mmap_arg_struct;
#[repr(C)] pub struct struct_sched_attr;
#[repr(C)] pub struct struct_sched_param;
#[repr(C)] pub struct struct_sel_arg_struct;
#[repr(C)] pub struct struct_sembuf;
#[repr(C)] pub struct struct_shmid_ds;
#[repr(C)] pub struct struct_sigaction;
#[repr(C)] pub struct struct_sigevent;
#[repr(C)] pub struct struct_siginfo;
#[repr(C)] pub struct struct_sigstack;
#[repr(C)] pub struct struct_sockaddr;
#[repr(C)] pub struct struct_stat64;
#[repr(C)] pub struct struct_stat64_emu31;
#[repr(C)] pub struct struct_stat;
#[repr(C)] pub struct struct_statfs64;
#[repr(C)] pub struct struct_statfs;
#[repr(C)] pub struct struct_sysinfo;
#[repr(C)] pub struct struct_timespec;
#[repr(C)] pub struct struct_timeval32;
#[repr(C)] pub struct struct_timeval;
#[repr(C)] pub struct struct_timex32;
#[repr(C)] pub struct struct_timex;
#[repr(C)] pub struct struct_timezone;
#[repr(C)] pub struct struct_tms;
#[repr(C)] pub struct struct_user_desc;
#[repr(C)] pub struct struct_user_msghdr;
#[repr(C)] pub struct struct_ustat;
#[repr(C)] pub struct struct_utimbuf;
#[repr(C)] pub struct struct_vm86_struct;
#[repr(C)] pub struct union_pl_args;
#[repr(C)] pub struct union_opf_attr;
#[repr(C)] pub struct union_bpf_attr;
#[repr(C)] pub struct void;

#[repr(C)] pub struct SigInfo;
#[repr(C)] pub struct Stack;
#[repr(C)] pub struct CompatStack;
#[repr(C)] pub struct SigSet;
#[repr(C)] pub struct CompatSigSet;
#[repr(C)] pub struct CapUserData;
#[repr(C)] pub struct CapUserHeader;
#[repr(C)] pub struct FDSet;
#[repr(C)] pub struct CompatMode;

#[repr(C)]
pub enum enum_pl_code { 
    PL_SET = 1,
    PL_FSET = 2,
    PL_GET = 3,
    PL_FGET = 4,
    PL_DEL = 5,
    PL_FDEL = 6,
}
